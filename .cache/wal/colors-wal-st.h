const char *colorname[] = {

  /* 8 normal colors */
  [0] = "#0D1E33", /* black   */
  [1] = "#3473AC", /* red     */
  [2] = "#457BA2", /* green   */
  [3] = "#498DB8", /* yellow  */
  [4] = "#4894C6", /* blue    */
  [5] = "#59A9D2", /* magenta */
  [6] = "#5BAFD5", /* cyan    */
  [7] = "#c6e0e8", /* white   */

  /* 8 bright colors */
  [8]  = "#8a9ca2",  /* black   */
  [9]  = "#3473AC",  /* red     */
  [10] = "#457BA2", /* green   */
  [11] = "#498DB8", /* yellow  */
  [12] = "#4894C6", /* blue    */
  [13] = "#59A9D2", /* magenta */
  [14] = "#5BAFD5", /* cyan    */
  [15] = "#c6e0e8", /* white   */

  /* special colors */
  [256] = "#0D1E33", /* background */
  [257] = "#c6e0e8", /* foreground */
  [258] = "#c6e0e8",     /* cursor */
};

/* Default colors (colorname index)
 * foreground, background, cursor */
 unsigned int defaultbg = 0;
 unsigned int defaultfg = 257;
 unsigned int defaultcs = 258;
 unsigned int defaultrcs= 258;
