static const char* selbgcolor   = "#0D1E33";
static const char* selfgcolor   = "#c6e0e8";
static const char* normbgcolor  = "#457BA2";
static const char* normfgcolor  = "#c6e0e8";
static const char* urgbgcolor   = "#3473AC";
static const char* urgfgcolor   = "#c6e0e8";
