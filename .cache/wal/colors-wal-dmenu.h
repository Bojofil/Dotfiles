static const char *colors[SchemeLast][2] = {
	/*     fg         bg       */
	[SchemeNorm] = { "#c6e0e8", "#0D1E33" },
	[SchemeSel] = { "#c6e0e8", "#3473AC" },
	[SchemeOut] = { "#c6e0e8", "#5BAFD5" },
};
