# Dotfiles - Bojofil

This repository houses the dotfiles used on my main production machine.  My current build is as follows:
- Distribution: Arch Linux
- Window Manager: Qtile
- Shell: Bash
- Terminal Emulator: st
- Bash Prompt: oh-my-bash!
- Text editor: code-oss (GUI) / VIM (CLI)
- File Manager: Thunar / Ranger
- Compositor: Compton-Tryone
- Colorscheming: pywal
## Installation

In order to use these files, simply clone the repository and place the files in their necessary directory.
```bash
git clone https://gitlab.com/Bojofil/Dotfiles.git
```

## Usage
Feel free to take these dotfiles and modify / use them however you would like.

Please note, some programs such as Polybar rely heavily on your personal system configuration and may not work right out of the box.  Make sure to check over some of the configuration files so you can change things to fit your system. 

Thanks!
